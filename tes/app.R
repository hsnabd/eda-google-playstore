library(shiny)
library(DiagrammeR)
library(magrittr)

##------------------------------------------
## ui function

ui <- shinyUI(fluidPage( 
  fluidRow(
    column(
      width=3,
      textInput('x', 'Node Name', 'a'),
      sliderInput(inputId = "height", label = "Height", min = 0, max = 2000, value = 300, step = 200),
      sliderInput(inputId = "width", label = "Width", min = 0, max = 2000, value = 300, step = 200)
    ),
    column(
      width = 8,
      uiOutput('d')
    )
  )
)
)


##------------------------------------------
## server function

server <- function(input, output){
  
  output$diagram <- renderGrViz({
    
    create_nodes(nodes = c(input$x, "b", "c", "d")) %>%
      graphviz_graph  %>% 
      graphviz_render(output = "DOT") %>%
      grViz
    
  })
  
  output$d <- renderUI({
    grVizOutput('diagram', height = input$height, width = input$width)
  })
  
}


##------------------------------------------
## run app

shinyApp(ui = ui, server = server)


## R version 3.1.2 (2014-10-31)
## Platform: x86_64-w64-mingw32/x64 (64-bit)
## magrittr_1.5      DiagrammeR_0.5    shiny_0.11.1.9004
## Firefox 37.0.2