# Exploratory Data Google Play Stores

Berikut adalah sebuah __dashboard project__ dengan `Rshiny`. Data yang digunakan berasal dari [kaggle dataset](https://www.kaggle.com/lava18/google-play-store-apps). Data tersebut adalah hasil scrapping 10k Aplikasi di PlayStore. Terdapat 13 fitur/variabel, yaitu:
1. __Apps__: Nama Aplikasi.
2. __Category__: Kategori klaster sasaran pengguna Aplikasi.
3. __Rating__: Rating seluruh pengguna.
4. __Reviews__: Jumlah ulasan dari pengguna Aplikasi.
5. __Size__: Ukuran Aplikasi.
6. __Install__: Jumlah pengguna yang mendownload.
7. __Type__: _Paid_ or _Free_
8. __Price__: Harga dari Aplikasi
9. __Content Rating__: Klaster usia target Aplikasi.
10. __Genres__: Kategori _genre_ tiap Aplikasi.
11. __Last Updated__: Tanggal ketika Aplikasi terakhir di perbarui.
12. __Current Version__: Versi terakhir dari Aplikasi yang tersedia di _Play Store_.
12. __Android Ver__: Minimum Versi Android yang dibutuhkan.

## Dashboard Content
Terdapat 4 __Item Menu__ antara lain:
1. `Exploratory Data Visualization`: berisi tentang _insight_ data yang presentasikan dengan grafik visualisasi.
2. `Sentiment Analysis`: berisi tentang uraian hasil  __sentiment analysis__ yang menyampaikan informasi deskriptif total review pengguna aplikasi __Google PlayStore__.
3. `Hierarchical Clustering`: berisi informasi hasil klastering menggunakan jarak `euclidean` dan metode `ward.D2`. Fitur yang digunakan yaitu, __Install__, __Reviews__,__Rating__.
4. `Data Table`: berisi informasi dataset yang digunakan dan `data table` hasil `subsetting`.


##  Fitur Selanjutnya

1. Menambahkan explanatory text pada tiap grafik untuk menginterpretasi hasil analisisnya.
2. Pada Menu `Hierarchical Clustering`, menambahkan __summary__ tiap klaster yang terbentuk.
3. Pada Menu `Sentiment Analysis` menambahkan `text input` sebagai penguji model _classifier_ yang terbentuk.
